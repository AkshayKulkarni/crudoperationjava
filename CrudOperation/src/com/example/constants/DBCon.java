package com.example.constants;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import com.example.pojo.User;

public class DBCon {
	public static Connection getConnection() throws ClassNotFoundException, SQLException {
		Connection con= null;
		Class.forName("com.mysql.cj.jdbc.Driver");
		con=DriverManager.getConnection("jdbc:mysql://localhost:3306/dbuser?useSSL=false","root","root");
		System.out.println("connected");
		return con;
	}
	
	public static Map<String, String> saveData(User u) throws ClassNotFoundException, SQLException {
		Connection con = getConnection();

		PreparedStatement ps = con.prepareStatement("insert into tbluser(firstname,lastname,email,pass) values(?,?,?,?)");
		String firstname= u.getFirstname();
		System.out.println(firstname);
		String lastname=u.getLastname();
		String email=u.getEmailId();
		String pass=u.getPassword();
		
		ps.setObject(1, firstname);
		ps.setObject(2, lastname);
		ps.setObject(3, email);
		ps.setObject(4, pass);
		
		int status= ps.executeUpdate();
		
		Map<String,String> map=new HashMap<>();
		
		if(status==1) {
			map.put("msg","done");
		}else {
			map.put("msg","sorry");
		}
		return map;
	}
	
	public static ArrayList<User> getUserDataFromDB() throws ClassNotFoundException, SQLException {

		Connection con = getConnection();

		PreparedStatement ps = con.prepareStatement("select * from tbluser");

		ResultSet rs = ps.executeQuery();

		ArrayList<User> list = new ArrayList<>();

		while (rs.next()) {
			User u = new User();
			u.setId(rs.getInt(1));
			u.setFirstname(rs.getString(2));
			u.setLastname(rs.getString(3));
			u.setPassword(rs.getString(5));
			u.setEmailId(rs.getString(4));
			list.add(u);
		}
		return list;
	}
	public static Map<String, String> deleteEmpData(User u) throws ClassNotFoundException, SQLException {
		
		Connection con = getConnection();

		PreparedStatement ps = con.prepareStatement("delete from tbluser where id=?");

		ps.setObject(1, u.getId());

		int status = ps.executeUpdate();

		Map<String, String> mp = new HashMap<>();

		if (status == 1) {
			mp.put("msg", "done");
		} else {
			mp.put("msg", "sorry");
		}

		return mp;

	}
	public static Map<String, String> updateData(User u) {

		Map<String, String> map=new HashMap<String, String>();
		
		try {
			Connection con = getConnection();

			PreparedStatement ps = con.prepareStatement("update tbluser set firstname=?,lastname=?,email=?,pass=? where id=?");

			ps.setString(1, u.getFirstname());
			ps.setString(2, u.getLastname());
			ps.setString(3, u.getEmailId());
			ps.setString(4, u.getPassword());
			ps.setInt(5, u.getId());

			int status = ps.executeUpdate();

			if (status == 1)
			{
				map.put("msg", "done");
			}
			else
			{
				map.put("msg", "sorry");
			}

		} 
		catch (Exception ex)
		{
			ex.printStackTrace();
		}

		return map;

	}


}
